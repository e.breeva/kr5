def zap():
    import random
    import copy
    n=int(input("n = "))
    matrd=[]
    matrcv=[]

    i=0
    while i<n:
        j=0
        d=[]
        while j<n:
            d.append(random.uniform(-100,100))
            j+=1
        matrcv.append(random.uniform(-100,100))
        matrd.append(copy.deepcopy(d))
        i+=1
    return(matrd,matrcv,n)
