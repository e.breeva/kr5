import matplotlib.pyplot as plt
import csv
def repl(s):
    return(s.replace('.',','))
def save(tim,razm,n):
    try:
        tim=str(tim).replace()
    except:
        tim=str(tim)
    razm=str(razm)
    try:
        file=open('dlyagrafGaus.csv','x')
    except:
        file=open("dlyagrafGaus.csv",'a',newline="")
    file_writer=csv.writer(file,delimiter=";",lineterminator="\r",)    
    file_writer.writerow([razm,tim,n])
    file.close()
    print("file save")
def loadgraf():
    gry1=[]
    gry2=[]
    grx=[] 
    file=open("dlyagrafGaus.csv")
    file_reader=csv.reader(file,delimiter=";")
    for row in file_reader:
        gry1.append(int(row[0]))
        gry2.append(float(row[1].replace(',','.')))
        grx.append(int(row[2]))
    file.close()
    plt.xlabel('размерность системы')
    plt.ylabel('объем занимаемой памяти, кб ')
    plt.grid()
    plt.plot(grx,gry1)
    plt.show()
    plt.ylabel('время выполнения, с')
    plt.xlabel('размерность системы,n*n')
    plt.grid()
    plt.plot(grx,gry2)
    plt.show()
