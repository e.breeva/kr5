import time
import copy
import tracemalloc
def gaus(matrd,matrcv,c):
    tracemalloc.start()
    d=copy.deepcopy(matrd)
    cv=copy.deepcopy(matrcv)
    matrd=copy.deepcopy(matrd)
    matrcv=copy.deepcopy(matrcv)
    k=0    
    start_time=time.time()
    while k<c-1:
        if matrd[k][k]==0:
            i=k+1
            count=1
            while i<c:
                if matrd[i][k]==0:
                    i+=1
                else:
                    temp=copy.deepcopy(matrd[i])
                    temp1=copy.deepcopy(matrcv[i])
                    break
        else:
                i=k
                temp=copy.deepcopy(matrd[k])
                temp1=copy.deepcopy(matrcv[k])
        matrd[i]=copy.deepcopy(matrd[k])
        matrd[k]=copy.deepcopy(temp)
        matrcv[i]=copy.deepcopy(matrcv[k])
        matrcv[k]=copy.deepcopy(temp1)
        d=copy.deepcopy(matrd)
        cv=copy.deepcopy(matrcv)
        kof=copy.deepcopy(d[k][k])
        j=0
        i=k
        while j<c:
                matrd[i][j]=matrd[i][j]/kof
                j+=1
        matrcv[i]=matrcv[i]/kof
        d=copy.deepcopy(matrd)
        cv=copy.deepcopy(matrcv)
        i=k+1
        while i<c:
                kof=d[i][k]
                j=0
                while j<c:
                    matrd[i][j]=matrd[i][j]-matrd[k][j]*kof
                    j+=1
                matrcv[i]=matrcv[i]-matrcv[k]*kof
                d=copy.deepcopy(matrd)
                cv=copy.deepcopy(matrcv)
                i+=1
        k+=1
    k=c-1
    while k>0:
        kof=copy.deepcopy(d[k][k])
        j=0
        i=k
        while j<c:
                matrd[i][j]=matrd[i][j]/kof
                j+=1
        matrcv[i]=matrcv[i]/kof
        d=copy.deepcopy(matrd)
        cv=copy.deepcopy(matrcv)
        i=k-1
        while i>=0:
                kof=d[i][k]
                j=0
                while j<c:
                    matrd[i][j]=matrd[i][j]-matrd[k][j]*kof
                    j+=1
                matrcv[i]=matrcv[i]-matrcv[k]*kof
                d=copy.deepcopy(matrd)
                cv=copy.deepcopy(matrcv)
                i-=1
        k-=1
    end_time=time.time()
    t=end_time-start_time
    first_size,first_peak=tracemalloc.get_traced_memory()

    return(d,cv,t,first_peak)
