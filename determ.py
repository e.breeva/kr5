import copy
def minor(matr,i,j):
    M=copy.deepcopy(matr)
    del M[i]
    for i in range(len(matr[0])-1):
        del M[i][j]
    return M
def det(matr):
    m=len(matr)
    n=len(matr[0])
    if m!=n:
        return None
    if n==1:
        return matr[0][0]
    signum=1
    determ=0
    for j in range(n):
        determ+=matr[0][j]*signum*det(minor(matr,0,j))
        signum*=-1
    return determ
